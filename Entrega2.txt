EJERCICIOS 2 - SEGUNDA ENTREGA

1)	Complete la implementaci�n ListaOrdImp2 del TAD ListaOrd especificado en el proyecto, 
	de forma que las operaciones AgregarOrd, Borrar, Recuperar y Existe se realicen en 
	tiempo de ejecuci�n O(log2n) en promedio y CantidadElementos en O(1). La implementaci�n no debe ser acotada.

2)	Complete la implementaci�n TablaImp del TAD Tabla especificado en el proyecto.
	La implementaci�n no debe ser acotada.
	Sugerencia: Usar la clase Asociacion (se puede hacer sin usarla)

3)	Complete la implementaci�n ColaPrioridadImp del TAD ColaPrioridad especificado en el proyecto.
	La implementaci�n no debe ser acotada.

4)	Implemente la funci�n ContarOcurrencias que retorne una tabla que contenga los pares <elemento, cantidad de ocurrencias> 
	para cada elemento de la lista l recibida.
	Ejemplo: 
	Dada la lista (1,5,9,1,2,9,9) retorna la tabla con los pares [1,2],[2,1],[5,1],[9,3]

	Tabla<T, int>* ContarOcurrencias(const ListaOrd<T>& l);

5)	Implemente la funci�n Simetricas que retorne true si y solo si 
	todos los dominios de t1 se encuentran como rangos en t2 y 
	todos los dominios de t2 se encuentran como rangos en t1.
	Pre: Los rangos no se repiten dentro de la tabla (es inyectiva)
	No se puede usar una implementacion para resolver el ejercicio (no se permite usar una clase terminada en Imp).                         

	Por ejemplo:
	t1 = {D:1|R:5} {D:3|R:9} {D:8|R:1} {D:10|R:6} 
	t2 = {D:1|R:8} {D:5|R:1} {D:6|R:10} {D:9|R:3} 
	retorno = true

	t1 = {D:1|R:5} {D:3|R:9} {D:8|R:1} {D:10|R:6} 
	t2 = {D:1|R:8} {D:5|R:1} {D:9|R:10} {D:6|R:3} 
	retorno = true

	t1 = {D:1|R:5} {D:3|R:9} {D:8|R:1} 
	t2 = {D:1|R:8} {D:5|R:1} {D:6|R:10} {D:9|R:3} 
	retorno = false

	t1 = {D:1|R:5} {D:3|R:9} {D:8|R:1} {D:10|R:6} 
	t2 = {D:1|R:20} {D:5|R:1} {D:6|R:10} {D:9|R:3} 
	retorno = false
	
	template <class D, class R>
	bool Simetricas(const Tabla<D,R> &t1, const Tabla<R,D> &t2);

5)	Implemente una funci�n MenorPrioridad que dada una cola de prioridad retorne una nueva cola de prioridad que solamente contenga
	los elementos que tengan la menor prioridad (varios elementos pueden tener la misma prioridad pero distinto orden de llegada).
	Si la cola es vacia entonces se debe retornar una cola vacia.
	No se puede usar otro TAD ni otras estructuras auxiliares para resolver el ejercicio.     
	No se puede usar una implementacion para resolver el ejercicio (no se permite usar una clase terminada en Imp).                         

	Considere el siguiente ejemplo donde E es el elemento encolado y P su prioridad. 
	(Los primeros que se muestran son los primeros en decolar)
	c = {E:"mama1"|P:7} {E:"mama2"|P:7} {E:"cliente1"|P:6} {E:"empleado1"|P:2} {E:"empleado2"|P:2} 
	retorno = {E:"empleado1"|P:2} {E:"empleado2"|P:2} 

	template <class T>
	ColaPrioridad<T, int>* MenorPrioridad(const ColaPrioridad<T, int> &c);

7)	Implemente la funci�n ParentesisBalanceados que dado un string con par�ntesis que se abren "(, [ o {" y se cierran "), ] o }",
	retorne true si y solo si se cumple que:
	- para cada par�ntesis abierto hay un par�ntesis que lo cierra. El par�ntesis cierra solamente al �ltimo abierto de su mismo tipo.
	- no hay par�ntesis que se cierren cuando no hay abiertos anteriores de su mismo tipo
	Ejemplo: "((5)+(7*7))*((4*8)/(2/2))" -> true
	Ejemplo: "(((4))" -> false
	Ejemplo: "(()4()()))" -> false
	Ejemplo: "(((4)})" -> false
	Ejemplo: "({(4)})" -> true

	bool ParentesisBalanceados(const char *formula);

8)	Considere la siguiente definici�n de �rbol general implementado como AB primer hijo - siguiente hermano de enteros:
	struct NodoAG { 
		int dato;
		NodoAG *ph, *sh; 
	};
	Implemente un procedimiento iterativo ImprimirPorNiveles, que recibiendo un puntero a un AG
	de tipo NodoAG lo muestre por consola por niveles, de arriba hacia abajo y de izquierda a derecha. 
	Se deben imprimir los valores separados por un espacio en blanco.
	El procedimiento puede recorrer como m�ximo una vez el �rbol par�metro. 

	void ImprimirPorNiveles(NodoAG* a)

	Por ejemplo recibiendo el AG {{1,-7,5,#,15,#,#,3,#,4,8,34}} se debe imprimir:
	1 -7 3 4 5 15 8 34
	Si el AG es vac�o no imprime nada.
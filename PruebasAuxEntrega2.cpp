#include "PruebasAuxEntrega2.h"


void pruebaContarOcurrencias(const char *lista) {

	int largo;
	NodoListaS* listaParseada = (NodoListaS*)FrameworkA1::parsearColeccion(lista, largo);

	ListaOrd<int> *listaOrd = FrameworkA1::ConvertirAListaOrd(listaParseada);

	Tabla<int, int> *tabla = ContarOcurrencias(*listaOrd);

	cout << *tabla << endl;

	FrameworkA1::destruir(listaParseada);
	delete listaOrd;
	delete tabla;
}

void pruebaSimetricas(const char* inputTabla1Dominio, const char* inputTabla1Rango, const char* inputTabla2Dominio, const char* inputTabla2Rango)
{
	int largoLista;
	NodoListaS* listaD1 = (NodoListaS*)FrameworkA1::parsearColeccion(inputTabla1Dominio, largoLista);
	NodoListaS* listaR1 = (NodoListaS*)FrameworkA1::parsearColeccion(inputTabla1Rango, largoLista);
	NodoListaS* listaD2 = (NodoListaS*)FrameworkA1::parsearColeccion(inputTabla2Dominio, largoLista);
	NodoListaS* listaR2 = (NodoListaS*)FrameworkA1::parsearColeccion(inputTabla2Rango, largoLista);

	Tabla<int, int> *t1 = FrameworkA1::ConvertirATabla(listaD1, listaR1);
	Tabla<int, int> *t2 = FrameworkA1::ConvertirATabla(listaD2, listaR2);

	bool res = Simetricas<int, int>(*t1, *t2);

	if (res)
	{
		cout << "True" << endl;
	}
	else
	{
		cout << "False" << endl;
	}

	FrameworkA1::destruir(listaD1);
	FrameworkA1::destruir(listaR1);
	FrameworkA1::destruir(listaD2);
	FrameworkA1::destruir(listaR2);
	delete t1;
	delete t2;
}

void pruebaMenorPrioridad(const char* inputCpElem, const char* inputCpPrio)
{
	int largoLista;
	NodoListaS* cp1elem = (NodoListaS*)FrameworkA1::parsearColeccion(inputCpElem, largoLista);
	NodoListaS* cp1prio = (NodoListaS*)FrameworkA1::parsearColeccion(inputCpPrio, largoLista);

	ColaPrioridad<int, int> *cp1 = FrameworkA1::ConvertirAColaPrioridad(cp1elem, cp1prio);

	ColaPrioridad<int, int> *res = MenorPrioridad<int>(*cp1);

	if (res == NULL)
		cout << "NULL" << endl;
	else
		cout << *res << endl;

	FrameworkA1::destruir(cp1elem);
	FrameworkA1::destruir(cp1prio);
	delete cp1;
	delete res;
}


void pruebaParentesisBalanceados(const char *formula)
{
	char *formulaCopia = FrameworkA1::copioString(formula);

	bool res = ParentesisBalanceados(formula);

	bool parametrosModificados = !FrameworkA1::sonIguales(formula, formulaCopia);

	if(parametrosModificados) {
		cout << "ERROR parametro modificado" << endl;
	} 
	else
	{
		if (res)
		{
			cout << "Estan balanceados" << endl;			
		}
		else
		{
			cout << "No estan balanceados" << endl;			
		}
	}

	delete [] formulaCopia;
}

void pruebaImprimirPorNivelesAG(const char* inputTree)
{
	int largo;
	NodoAG* arbol = (NodoAG*)FrameworkA1::parsearColeccion(inputTree, largo);

	ImprimirPorNiveles(arbol);
	FrameworkA1::destruir(arbol);
}


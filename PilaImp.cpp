#include "PilaImp.h"

#ifndef PILAIMP_CPP
#define PILAIMP_CPP

template <class T>
inline ostream &operator<<(ostream& out, const PilaImp<T> &c) {
	c.Imprimir(out);
	return out;
}

template <class T>
PilaImp<T>::PilaImp() {
	this->usado = 0;
	this->tope = NULL;
}

template<class T>
PilaImp<T>::PilaImp(const Pila<T>& p) {
	this->tope = NULL;
	this->usado = 0;
	*this = p;
}

template<class T>
PilaImp<T>::PilaImp(const PilaImp<T>& p) {
	this->usado = 0;
	this->tope = NULL;
	*this = p;
}

template<class T>
Pila<T>& PilaImp<T>::operator=(const Pila<T>& p) {
	Pila<T>* cursor1 = p.Clon();
	PilaImp<T> * cursor2 = new PilaImp<T>();
	this->Vaciar();
	while (!cursor1->EsVacia()) {
		cursor2->Push(cursor1->Pop());
	}
	while (!cursor2->EsVacia()) {
		this->Push(cursor2->Pop());
	}
	return *this;
}

template<class T>
Pila<T>& PilaImp<T>::operator=(const PilaImp<T>& p) {
	PilaImp<T>* cursor1 = p.Clon();
	PilaImp<T> * cursor2 = new PilaImp<T>();
	this->Vaciar();
	while (!cursor1->EsVacia()) {
		cursor2->Push(cursor1.Pop());
	}
	while (!cursor2->EsVacia()) {
		this->Push(cursor2->Pop());
	}
	return *this;
}

template<class T>
bool PilaImp<T>::operator==(const Pila<T> &p) const {
	bool respuesta = this->CantidadElementos() == p.CantidadElementos();
	if (respuesta) {
		Pila<T> * cursor1 = this->Clon();
		Pila<T> * cursor2 = p.Clon();
		while (respuesta && !cursor1->EsVacia() && !cursor2->EsVacia()) {
			respuesta = respuesta && cursor1->Pop() == cursor2->Pop();
		}
	}
	return respuesta;
}

template<class T>
PilaImp<T>::~PilaImp() {
	this->Vaciar();
}

template<class T>
Pila<T>* PilaImp<T>::CrearVacia() const {
	return new PilaImp<T>();
}

template<class T>
void PilaImp<T>::Push(const T& e) {
	NodoPila * nuevo = new NodoPila();
	nuevo->dato = e;
	nuevo->sig = this->tope;
	this->tope = nuevo;
	this->usado++;
}

template<class T>
T& PilaImp<T>::Top() const {
	return this->tope->dato;
}

template<class T>
T PilaImp<T>::Pop() {
	T respuesta = this->tope->dato;
	this->tope = this->tope->sig;
	this->usado--;
	return respuesta;
}

template<class T>
void PilaImp<T>::Vaciar() {
	while (!this->EsVacia()) {
		this->Pop();
	}
}

template<class T>
unsigned int PilaImp<T>::CantidadElementos() const {
	return this->usado;
}

template<class T>
bool PilaImp<T>::EsVacia() const {
	return this->usado == 0;
}

template <class T>
bool PilaImp<T>::EsLlena() const {
	return false;
}

template<class T>
Pila<T>* PilaImp<T>::Clon() const {
	PilaImp<T>* respuesta = new PilaImp<T>();
	NodoPila * cursor1 = this->tope;
	NodoPila * cursor2;

	respuesta->usado = this->usado;
	respuesta->tope = this->tope;
	cursor2 = respuesta->tope;

	while (cursor1 != NULL) {
		cursor1 = cursor1->sig;
		cursor2 = cursor2->sig;
		cursor2 = cursor1;
	}
	
	return respuesta;
}

template<class T>
void PilaImp<T>::Imprimir(ostream & o) const
{
	if (!this->EsVacia()) {
		unsigned int cant = this->usado;
		NodoPila * aux = this->tope;
		while (aux != NULL) {
			o << aux->dato;
			if (cant > 1) {
				o << " ";
			}
			aux = aux->sig;
			cant--;
		}
	}
}
#endif // !PILAIMP_CPP
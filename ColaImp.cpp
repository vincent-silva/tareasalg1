#include "ColaImp.h"

#ifndef COLAIMP_CPP
#define COLAIMP_CPP

template <class T>
ostream &operator<<(ostream& out, const ColaImp<T> &c) {
	c.Imprimir(out);
	return out;
}

template <class T>
ColaImp<T>::ColaImp(){
	this->largo = 0;
	this->inicio = 0;
	this->fin = 0;
}

template <class T>
ColaImp<T>::ColaImp(const Cola<T> &c){
	this->ppio = NULL;
	this->fin = NULL;
	this->largo = 0;
	*this = c;
	return *this;
}

template <class T>
ColaImp<T>::ColaImp(const ColaImp<T> &c){
	this->ppio = NULL;
	this->fin = NULL;
	this->largo = 0;
	*this = c;
	return *this;
}

template <class T>
Cola<T> & ColaImp<T>::operator=(const Cola<T> &c){
	Cola<T> * cursor = c.Clon();
	this->Vaciar();
	while (!cursor->EsVacia()) {
		this->Encolar(cursor->Desencolar());
	}
	return *this;
}

template <class T>
Cola<T> & ColaImp<T>::operator=(const ColaImp<T> &c){
	ColaImp<T> * cursor = c.Clon();
	this->Vaciar();
	while (!cursor->EsVacia()) {
		this->Encolar(cursor->Desencolar());
	}
	return *this;
}

template <class T>
bool ColaImp<T>::operator==(const Cola<T>& c) const{
	bool respuesta = this->CantidadElementos() == c.CantidadElementos();
	if (respuesta) {
		Cola<T> * cursor1 = c.Clon();
		Cola<T> * cursor2 = this->Clon();
		while (!cursor1->EsVacia() && respuesta) {
			respuesta = respuesta && cursor1->Desencolar() == cursor2->Desencolar();
		}
	}
	return respuesta;
}

template <class T>
ColaImp<T>::~ColaImp(){
	this->Vaciar();
}

template<class T>
Cola<T>* ColaImp<T>::CrearVacia() const {
	return new ColaImp<T>();
}

template <class T>
void ColaImp<T>::Encolar(const T &e){
	NodoCola * nodo = new NodoCola;
	nodo->dato = e;
	nodo->sig = NULL;
	if (this->fin == NULL) {
		this->fin = nodo;
		this->inicio = this->fin;
	} else {
		this->fin->sig = nodo;
		this->fin = nodo;
	}
	this->largo++;
}

template <class T>
T& ColaImp<T>::Principio()const{
	return this->inicio->dato;
}

template <class T>
T ColaImp<T>::Desencolar(){
	T respuesta = this->inicio->dato;
	if (this->inicio != this->fin) {
		this->inicio = this->inicio->sig;
	} else {
		this->inicio = NULL;
		this->fin = NULL;
	}
	this->largo--;
	return respuesta;
}

template <class T>
void ColaImp<T>::Vaciar(){
	while (!this->EsVacia())
	{
		this->Desencolar();
	}
}

template <class T>
unsigned int ColaImp<T>::CantidadElementos()const{
	return this->largo;
}

template <class T>
bool ColaImp<T>::EsVacia() const{
	return this->CantidadElementos() == 0;
}

template <class T>
bool ColaImp<T>::EsLlena() const{
	return false;
}

template <class T>
Cola<T>* ColaImp<T>::Clon()const{
	ColaImp<T> * respuesta = new ColaImp<T>();
	NodoCola * cursor = this->inicio;
	while (cursor != NULL) {
		respuesta->Encolar(cursor->dato);
		cursor = cursor->sig;
	}
	return respuesta;
}

template <class T>
void ColaImp<T>::Imprimir(ostream& o)const{
	if (!this->EsVacia()) {
		NodoCola * cursor = this->inicio;
		while (cursor != NULL) {
			o << cursor->dato;
			cursor = cursor->sig;
			if (cursor != NULL) {
				o << " ";
			}
		}
	}
}

#endif
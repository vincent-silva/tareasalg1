
#ifndef ENTREGA2_H
#define ENTREGA2_H

#include "Definiciones.h"

#include "ColaImp.h"
#include "PilaImp.h"
#include "ListaOrd.h"
#include "TablaImp.h"
#include "MultiSet.h"
#include "ListaOrdImp.h"
#include "MultiSetImp.h"
#include "ColaPrioridad.h"
#include "ColaPrioridadImp.h"




template <class T>
Tabla<T, int> *ContarOcurrencias(const ListaOrd<T>& l)
{
	// NO IMPLEMENTADA
	return new TablaImp<T, int>();	
}


template <class D, class R>
bool Simetricas(const Tabla<D, R> &t1, const Tabla<R, D> &t2)
{
	// NO IMPLEMENTADA
	return false;	

}


template <class T>
ColaPrioridad<T, int>* MenorPrioridad(const ColaPrioridad<T, int> &c)
{
	// NO IMPLEMENTADA
	return c.CrearVacia();
}

bool ParentesisBalanceados(const char *formula);

void ImprimirPorNiveles(NodoAG* a);

#endif

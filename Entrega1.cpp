#include "Entrega1.h"

#ifndef ENTREGA1_CPP
#define ENTREGA1_CPP

ListaOrd<int>* Enlistar(NodoAB* a)
{
	ListaOrd<int> * respuesta = new ListaOrdImp<int>();
	ListaOrd<int> * aux = new ListaOrdImp<int>();
	if (a != NULL) {
		aux = Enlistar(a->izq);
		aux->AgregarOrd(a->dato);
		respuesta = UnionListaOrd(*aux, *Enlistar(a->der));
	}

	return respuesta;
}

unsigned int CantidadDeHojas(NodoAB* a)
{
	int respuesta = 0;
	Pila<NodoAB*> *pila = new PilaImp<NodoAB*>();
	NodoAB * aux;
	if (a != NULL) {
		pila->Push(a);
		while (!pila->EsVacia())
		{
			aux = pila->Pop();
			if (aux->izq == NULL && aux->der == NULL) {
				respuesta++;
			}
			else {
				if (aux->izq != NULL) {
					pila->Push(aux->izq);
				}
				if (aux->der != NULL) {
					pila->Push(aux->der);
				}
			}
		}
	}

	return respuesta;
}

void ImprimirPorNiveles(NodoAB *a)
{
	Cola<NodoAB*>* cola = new ColaImp<NodoAB*>();
	Pila<int>* pila = new PilaImp<int>();
	NodoAB * aux;
	if (a != NULL) {
		cola->Encolar(a);
		while (!cola->EsVacia()) {
			aux = cola->Desencolar();
			pila->Push(aux->dato);
			if (aux->izq != NULL) {
				cola->Encolar(aux->izq);
			}
			if (aux->der != NULL) {
				cola->Encolar(aux->der);
			}
		}

		while (!pila->EsVacia()) {
			cout << pila->Pop() << " ";
		}
	}
}

#endif
#include "ListaPosImp.h"

#ifndef LISTAPOSIMP_CPP
#define LISTAPOSIMP_CPP

template <class T>
inline ostream & operator<<(ostream &out, const ListaPosImp<T> &l)
{
	l.Imprimir(out);
	return out;
}

template <class T>
ListaPos<T>* ListaPosImp<T>::CrearVacia() const
{
	return new ListaPosImp<T>();
}

template <class T>
ListaPosImp<T>::ListaPosImp()
{
	this->Vaciar();
}

template <class T>
ListaPosImp<T>::ListaPosImp(const ListaPos<T> &l)
{
	this->Vaciar();
	for (int i = 0; i < l.CantidadElementos(); i++)
	{
		this->AgregarFin(l.ElementoPos(i));
	}
}

template <class T>
ListaPosImp<T>::ListaPosImp(const ListaPosImp<T> &l)
{
	this->Vaciar();
	for (int i = 0; i < l.CantidadElementos(); i++)
	{
		this->AgregarFin(l.ElementoPos(i));
	}
}

template <class T>
ListaPos<T>& ListaPosImp<T>::operator=(const ListaPos<T> &l)
{
	T val;
	if (this != &l) {
		this->Vaciar();
		for (Iterador<T> &i = l.GetIterador(); !i.EsFin();) {
			this->AgregarFin(i++);
		}
	}
	return *this;
}

template <class T>
ListaPos<T>& ListaPosImp<T>::operator=(const ListaPosImp<T> &l)
{
	if (this != &l) {
		this->Vaciar();
		for (Iterador<T> &i = l.GetIterador(); !i.EsFin();) {
			this->AgregarFin(i++);
		}
	}
	return *this;
}

template <class T>
ListaPosImp<T>::~ListaPosImp()
{
	this->Vaciar();
}

template <class T>
void ListaPosImp<T>::AgregarPpio(const T &e) 
{
	this->AgregarPos(e, 0);
}

template <class T>
void ListaPosImp<T>::AgregarFin(const T &e) 
{
	if (this->EsLlena()) {
		T* aux = this->elementos;
		this->largo += 20;
		this->elementos = new T[this->largo];
		for (int i = 0; i < this->usado; i++)
		{
			this->elementos[i] = aux[i];
		}
	}	
	this->elementos[this->usado] = e;
	this->usado++;
}

template <class T>
void ListaPosImp<T>::AgregarPos(const T &e, unsigned int pos)
{
	if (pos < this->usado) {
		AgregarPos(this->elementos[pos], pos + 1);
		this->elementos[pos] = e;
	} else {
		this->AgregarFin(e);
	}

}

template <class T>
void ListaPosImp<T>::BorrarPpio()
{
	this->BorrarPos(0);
}

template <class T>
void ListaPosImp<T>::BorrarFin()
{
	this->BorrarPos(this->usado - 1);
}

template <class T>
void ListaPosImp<T>::BorrarPos(unsigned int pos)
{
	if (pos < this->usado - 1) {
		this->elementos[pos] = this->elementos[pos + 1];
		this->BorrarPos(pos + 1);
	} else if (pos == this->usado - 1)
	{
		this->usado--;
	}
}

template <class T>
void ListaPosImp<T>::Borrar(const T &e)
{
	if (!EsVacia()) {
		this->BorrarPos(this->Posicion(e));
	}
}

template <class T>
T& ListaPosImp<T>::ElementoPpio() const
{
	return this->elementos[0];
}

template <class T>
T& ListaPosImp<T>::ElementoFin() const
{
	return this->elementos[usado - 1];
}

template <class T>
T& ListaPosImp<T>::ElementoPos(unsigned int pos) const
{
	if (!EsVacia()) {
		if (pos < usado) {
			return this->elementos[pos];
		}
		else {
			return this->elementos[usado - 1];
		}
	}
}

template <class T>
unsigned int ListaPosImp<T>::Posicion(const T &e) const
{
	int respuesta = -1;
	if (!EsVacia()) {
		for (int i = 0; i < usado && respuesta == -1; i++)
		{
			if (this->ElementoPos(i) == e) {
				respuesta = i;
			}
		}
	}
	return respuesta;
}

template <class T>
bool ListaPosImp<T>::Existe(const T &e) const
{
	bool respuesta = false;

	for (int i = 0; i < this->usado && !respuesta; i++)
	{
		respuesta = e == this->ElementoPos(i);
	}

	return respuesta;
}

template <class T>
void ListaPosImp<T>::Vaciar()
{
	while (this->usado > 0) {
		this->BorrarFin();
	}
	this->usado = 0;
	this->largo = 20;
	this->elementos = new T[largo];
}

template <class T>
unsigned int ListaPosImp<T>::CantidadElementos() const
{ 
	return this->usado;
}

template <class T>
bool ListaPosImp<T>::EsVacia() const
{
	return this->usado == 0;
}

template <class T>
bool ListaPosImp<T>::EsLlena() const
{
	return this->largo == this->usado;
}

template <class T>
ListaPos<T>* ListaPosImp<T>::Clon() const
{
	return new ListaPosImp<T>(*this);
}

template <class T>
Iterador<T> ListaPosImp<T>::GetIterador() const
{
	return IteradorListaPosImp<T>(*this);
}

template <class T>
void ListaPosImp<T>::Imprimir(ostream& o) const
{
	for (int i = 0; i < this->usado; i++)
	{
		if (i > 0) {
			o << " ";
		}
		o << this->ElementoPos(i);
	}
}

#endif
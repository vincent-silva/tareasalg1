
#include "PruebasEjemploDeUsoEntrega2.h"

PruebasEjemploDeUsoEntrega2::PruebasEjemploDeUsoEntrega2(ConductorPrueba* conductor)
:Prueba(conductor)
{

}

PruebasEjemploDeUsoEntrega2::~PruebasEjemploDeUsoEntrega2()
{
	
}

const char* PruebasEjemploDeUsoEntrega2::getNombre() const
{
	return "PruebasEjemploDeUsoEntrega2";
}

void PruebasEjemploDeUsoEntrega2::correrPruebaConcreta()
{
	PruebasEjerciciosTAD2();
	pruebaListaOrd();
	pruebaListaOrd2();
	pruebaTabla();
	pruebaColaPrioridad();
}

void PruebasEjemploDeUsoEntrega2::PruebasEjerciciosTAD2()
{


	// Pruebas ContarOcurrencias
	this->mImpresion.iniciarSeccion("PRUEBAS ContarOcurrencias");
	pruebaContarOcurrencias("(1, 1, 2)");
	ver3("Prueba ContarOcurrencias 1");
	pruebaContarOcurrencias("()");
	ver3("Prueba ContarOcurrencias 2");
	pruebaContarOcurrencias("(1, 2, 3, 2, 1)");
	ver3("Prueba ContarOcurrencias 3");
	pruebaContarOcurrencias("(10, 1, 1, 1, 1, 10, 5, 8)");
	ver3("Prueba ContarOcurrencias 4");
	pruebaContarOcurrencias("(1)");
	ver3("Prueba ContarOcurrencias 5");
	pruebaContarOcurrencias("(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)");
	ver3("Prueba ContarOcurrencias 6");
	pruebaContarOcurrencias("(1, 1, 1, 1, 1, 1)");
	ver3("Prueba ContarOcurrencias 7");
	pruebaContarOcurrencias("(1, 10, 100, 1000)");
	ver3("Prueba ContarOcurrencias 8");
	pruebaContarOcurrencias("(4, 6, 4, 6, 4, 6, 4, 6)");
	ver3("Prueba ContarOcurrencias 9");
	pruebaContarOcurrencias("(1, 2, 1, 4, 2, 1, 4, 2, 7)");
	ver3("Prueba ContarOcurrencias 10");
	this->mImpresion.cerrarSeccion("PRUEBAS ContarOcurrencias");

	this->mImpresion.iniciarSeccion("PRUEBAS pruebaSimetricas");
	pruebaSimetricas("(1,2,3)", "(5,4,6)", "(4,5,6)", "(1,3,2)");
	ver3("Prueba pruebaSimetricas 1");
	pruebaSimetricas("(1)", "(2)", "(1)", "(2)");
	ver3("Prueba pruebaSimetricas 2");
	pruebaSimetricas("(1,2,3,4)", "(5,6,7,8)", "(6,7,8,5)", "(4,3,2,1)");
	ver3("Prueba pruebaSimetricas 3");
	pruebaSimetricas("(2)", "(1)", "(1)", "(2)");
	ver3("Prueba pruebaSimetricas 4");
	pruebaSimetricas("(1)", "(2)", "(3,2)", "(1,2)");
	ver3("Prueba pruebaSimetricas 5");
	pruebaSimetricas("()", "()", "()", "()");
	ver3("Prueba pruebaSimetricas 6");
	pruebaSimetricas("(1,2,3,4)", "(1,2,3,4)", "(1)", "(2)");
	ver3("Prueba pruebaSimetricas 7");
	pruebaSimetricas("(1)", "(1)", "()", "()");
	ver3("Prueba pruebaSimetricas 8");
	pruebaSimetricas("(1)", "(1)", "(1)", "(1)");
	ver3("Prueba pruebaSimetricas 9");
	pruebaSimetricas("(1,2,3,4)", "(5,6,7,8)", "(5,6,7,8)", "(1,2,3,4)");
	ver3("Prueba pruebaSimetricas 10");
	pruebaSimetricas("(1,2)", "(3,4)", "(4,3)", "(2,1)");
	ver3("Prueba pruebaSimetricas 11");
	this->mImpresion.cerrarSeccion("PRUEBAS pruebaSimetricas");


	this->mImpresion.iniciarSeccion("PRUEBAS pruebaMenorPrioridad");
	pruebaMenorPrioridad("()", "()");
	ver3("Prueba pruebaMenorPrioridad 1");
	pruebaMenorPrioridad("(4,2,3,1)", "(1,3,2,4)");
	ver3("Prueba pruebaMenorPrioridad 2");
	pruebaMenorPrioridad("(5,9,2,-3,5,7,9,-2,1,-1)", "(5,6,2,3,8,2,4,-4,2,-1)");
	ver3("Prueba pruebaMenorPrioridad 3");
	pruebaMenorPrioridad("(6,30,4,-10,9)", "(-1,0,7,-1,9)");
	ver3("Prueba pruebaMenorPrioridad 4");
	pruebaMenorPrioridad("(15,-4,3,-3,-2,12,5,7,6,9,-3,-5,1)", "(5,4,4,3,2,2,2,1,2,4,3,5,1)");
	ver3("Prueba pruebaMenorPrioridad 5");
	pruebaMenorPrioridad("(2,2,4,4)", "(4,4,4,4)");
	ver3("Prueba pruebaMenorPrioridad 6");
	pruebaMenorPrioridad("(2,1,7,6,8)", "(4,4,5,4,4)");
	ver3("Prueba pruebaMenorPrioridad 7");
	pruebaMenorPrioridad("(9,8,7,5,3,2,5,4,1,3)", "(-1,-2,-3,-4,-5,-6,-7,-8,-9,-10)");
	ver3("Prueba pruebaMenorPrioridad 8");
	pruebaMenorPrioridad("(1)", "(-1)");
	ver3("Prueba pruebaMenorPrioridad 9");
	pruebaMenorPrioridad("(1,2,3,4,5,6,7,8,9)", "(1,-1,-2,1,-1,-2,-2,-1,1)");
	ver3("Prueba pruebaMenorPrioridad 10");
	this->mImpresion.cerrarSeccion("PRUEBAS pruebaMenorPrioridad");


	// Pruebas parentesisBalanceados
	this->mImpresion.iniciarSeccion("PRUEBAS Paréntesis Balanceados");
	pruebaParentesisBalanceados("{(1,3),(4,5)}++{[1,3,4]}");
	ver3("Prueba parentesisBalanceados 1");
	pruebaParentesisBalanceados("((5*2)+(2-a)+b)");
	ver3("Prueba parentesisBalanceados 2");	
	pruebaParentesisBalanceados("((a&&b)||((!c)&&(d&&!e)))");
	ver3("Prueba parentesisBalanceados 3");	
	pruebaParentesisBalanceados("((a&&b)||((!c)&&(d&&!e))");
	ver3("Prueba parentesisBalanceados 4");	
	pruebaParentesisBalanceados("((a&&b)||((!c)))&&)(d&&!e");
	ver3("Prueba parentesisBalanceados 5");	
	pruebaParentesisBalanceados("())(");
	ver3("Prueba parentesisBalanceados 6");	
	pruebaParentesisBalanceados("{(1,3),(4,5)}++1,3,4]}");
	ver3("Prueba parentesisBalanceados 7");	
	pruebaParentesisBalanceados("[(1+3){]}");
	ver3("Prueba parentesisBalanceados 8");	
	pruebaParentesisBalanceados("[(13)+{1,2,3}]");
	ver3("Prueba parentesisBalanceados 9");	
	pruebaParentesisBalanceados("{[((({}+{})))]()})");
	ver3("Prueba parentesisBalanceados 10");
	this->mImpresion.cerrarSeccion("PRUEBAS Paréntesis Balanceados");

	// Pruebas ImprimirPorNiveles
	this->mImpresion.iniciarSeccion("PRUEBAS ImprimirPorNiveles AG");
	pruebaImprimirPorNivelesAG("{{}}");
	ver3("Prueba imprimir AG por niveles 1");
	
	pruebaImprimirPorNivelesAG("{{1,-7,5,#,15,#,#,3,#,4,8,34}}");
	ver3("Prueba imprimir AG por niveles 2");

	pruebaImprimirPorNivelesAG("{{1,-7,5,1,#,#,15,#,#,3,#,4,8,34,#,#,7,24,13,#,7,1,#,#,#,#,2}}");
	ver3("Prueba imprimir AG por niveles 3");

	this->mImpresion.cerrarSeccion("PRUEBAS ImprimirPorNiveles AG");
}

void PruebasEjemploDeUsoEntrega2::pruebaListaOrd()
{	
	this->mImpresion.iniciarSeccion("PRUEBAS LISTA ORD");

	
	ListaOrd<int>* l = new ListaOrdImp<int>();


	//cout << "------------------- Prueba agregar elemento ----------------------" << endl << endl << endl;

	l->AgregarOrd(5);
	l->AgregarOrd(3);
	l->AgregarOrd(3);
	l->AgregarOrd(6);
	l->AgregarOrd(6);
	l->AgregarOrd(1);
	l->AgregarOrd(4);
	l->AgregarOrd(2);
	l->AgregarOrd(1);

	cout << *l <<endl;
	ver3("Imprimo la lista luego de agregar 9 datos");

	
	//cout<<"--------------------Prueba largo de la lista-------------------"<<endl<<endl<<endl;
	
	cout << l->CantidadElementos()<<endl;
	ver3("Imprimo el largo de la lista");

	
	//cout<<"--------------------Prueba clon de la lista-------------------"<<endl<<endl<<endl;
	
	
	ListaOrd<int>* clonL = l->Clon();

	cout << "Resultado Original: " << endl << *l<< endl << endl;

	cout << "Resultado Clon: " << endl << *clonL << endl;

	ver3("Imprimo la lista original y su clon");
	////////////////////////////////////////////////////////////////////////////////////////
	ListaOrd<int>* listaIgual = l->CrearVacia();
	*listaIgual = *l;

	cout << "Resultado Original: " << endl << *l<< endl << endl;

	cout << "Resultado lista obtenida operador =: " << endl << *listaIgual << endl;

	ver3("Imprimo la lista original y la obtenida");
	//cout<<"--------------------Prueba == de la lista (iguales)-------------------"<<endl<<endl<<endl;

	if(*l==*clonL)
	{
		cout<<"La lista original es igual a la lista clon"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista original no es igual a la lista clon"<<endl<<endl<<endl;
	}
	ver3("Imprimo prueba de == sobre listas iguales");

	//cout<<"--------------------Prueba == de la lista (diferentes)-------------------"<<endl<<endl<<endl;
	clonL->BorrarMinimo();

	if(*l==*clonL)
	{
		cout<<"La lista original es igual a la lista clon"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista original no es igual a la lista clon"<<endl<<endl<<endl;
	}

	ver3("Imprimo prueba de == sobre listas distintas");

	//cout<<"--------------------Prueba elemento principio-------------------"<<endl<<endl<<endl;
	

	cout << l->Minimo()<<endl;
	ver3("Imprimo el principio de la lista");

	
	//cout<<"--------------------Prueba elemento fin-------------------"<<endl<<endl<<endl;
	

	cout << l->Maximo() <<endl;
	ver3("Imprimo el fin de la lista");

	
	//cout<<"--------------------Prueba eliminar elemento principio-------------------"<<endl<<endl<<endl;
	
	l->BorrarMinimo();

	cout << *l <<endl;
	ver3("Imprimo la lista luego de eliminar el principio");

	//cout<<"--------------------Prueba eliminar elemento fin-------------------"<<endl<<endl<<endl;
	
	l->BorrarMaximo();

	cout << *l <<endl;
	ver3("Imprimo la lista luego de eliminar el fin");

	//cout<<"--------------------Prueba eliminar elemento-------------------"<<endl<<endl<<endl;
	
	l->Borrar(3);

	cout << *l <<endl;
	ver3("Imprimo la lista luego de eliminar el elemento 3");

	
	//cout<<"--------------------Prueba recuperar elemento-------------------"<<endl<<endl<<endl;
	
	cout << l->Recuperar(2)<<endl;
	
	ver3("Imprimo la recuperación del elemento 2");

	//cout<<"---------------------Prueba elemento pertenece ---------------------"<<endl<<endl<<endl;
	if(l->Existe(2))
	{
		cout<<"Elemento 2 pertenece  a la lista"<<endl<<endl<<endl;
	}else
	{
		cout<<"Elemento 2 no pertenece a la lista"<<endl<<endl<<endl;
	}
	ver3("Imprimo si el elemento 2 existe en la lista");
	//cout<<"---------------------Prueba elemento no pertenece ---------------------"<<endl<<endl<<endl;
	if(l->Existe(6))
	{
		cout<<"Elemento 6 pertenece  a la lista"<<endl<<endl<<endl;
	}else
	{
		cout<<"Elemento 6 no pertenece a la lista"<<endl<<endl<<endl;
	}

	ver3("Imprimo si el elemento 6 existe en la lista");
	//cout<<"-------------------Prueba lista es vacia ----------------------"<<endl<<endl<<endl;
	
	if(l->EsVacia())
	{
		cout<<"La lista se encuentra vacia"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista no se encuentra vacia"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista es vacia");
	/////////////////////////////////////////////////////////////////////////
	ListaOrd<int>* lVacia= l->CrearVacia();
	if(lVacia->EsVacia())
	{
		cout<<"La lista se encuentra vacia"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista no se encuentra vacia"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista creada es vacia ");
	delete lVacia;
	/////////////////////////////////////////////////////////////////////////
	if(l->EsLlena())
	{
		cout<<"La lista se encuentra llena"<<endl<<endl<<endl;
	}
	else
	{
		cout<<"La lista no se encuentra llena"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista esta llena");

	//cout<<"-------------------Prueba lista es vaciar ----------------------"<<endl<<endl<<endl;
	
	l->Vaciar();
	
	cout<<"["<<*l<<"]"<<endl<<endl<<endl;

	ver3("Imprimo la lista luego de vaciarla");

	//cout<<"-------------------Prueba lista es vacia ----------------------"<<endl<<endl<<endl;
	
	if(l->EsVacia())
	{
		cout<<"La lista se encuentra vacia"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista no se encuentra vacia"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista es vacia");
	delete l;
	delete clonL;

	this->mImpresion.cerrarSeccion("PRUEBAS LISTA ORD");
}

void PruebasEjemploDeUsoEntrega2::pruebaListaOrd2()
{	
	this->mImpresion.iniciarSeccion("PRUEBAS LISTA ORD");

	
	ListaOrd<int>* l = new ListaOrdImp2<int>();


	//cout << "------------------- Prueba agregar elemento ----------------------" << endl << endl << endl;

	l->AgregarOrd(5);
	l->AgregarOrd(3);
	l->AgregarOrd(3);
	l->AgregarOrd(6);
	l->AgregarOrd(6);
	l->AgregarOrd(1);
	l->AgregarOrd(4);
	l->AgregarOrd(2);
	l->AgregarOrd(1);

	cout << *l <<endl;
	ver3("Imprimo la lista luego de agregar 9 datos");

	
	//cout<<"--------------------Prueba largo de la lista-------------------"<<endl<<endl<<endl;
	
	cout << l->CantidadElementos()<<endl;
	ver3("Imprimo el largo de la lista");

	
	//cout<<"--------------------Prueba clon de la lista-------------------"<<endl<<endl<<endl;
	
	
	ListaOrd<int>* clonL = l->Clon();

	cout << "Resultado Original: " << endl << *l<< endl << endl;

	cout << "Resultado Clon: " << endl << *clonL << endl;

	ver3("Imprimo la lista original y su clon");
	////////////////////////////////////////////////////////////////////////////////////////
	ListaOrd<int>* listaIgual = l->CrearVacia();
	*listaIgual = *l;

	cout << "Resultado Original: " << endl << *l<< endl << endl;

	cout << "Resultado lista obtenida operador =: " << endl << *listaIgual << endl;

	ver3("Imprimo la lista original y la obtenida");
	//cout<<"--------------------Prueba == de la lista (iguales)-------------------"<<endl<<endl<<endl;

	if(*l==*clonL)
	{
		cout<<"La lista original es igual a la lista clon"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista original no es igual a la lista clon"<<endl<<endl<<endl;
	}
	ver3("Imprimo prueba de == sobre listas iguales");

	//cout<<"--------------------Prueba == de la lista (diferentes)-------------------"<<endl<<endl<<endl;
	clonL->BorrarMinimo();

	if(*l==*clonL)
	{
		cout<<"La lista original es igual a la lista clon"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista original no es igual a la lista clon"<<endl<<endl<<endl;
	}

	ver3("Imprimo prueba de == sobre listas distintas");

	//cout<<"--------------------Prueba elemento principio-------------------"<<endl<<endl<<endl;
	

	cout << l->Minimo()<<endl;
	ver3("Imprimo el principio de la lista");

	
	//cout<<"--------------------Prueba elemento fin-------------------"<<endl<<endl<<endl;
	

	cout << l->Maximo() <<endl;
	ver3("Imprimo el fin de la lista");

	
	//cout<<"--------------------Prueba eliminar elemento principio-------------------"<<endl<<endl<<endl;
	
	l->BorrarMinimo();

	cout << *l <<endl;
	ver3("Imprimo la lista luego de eliminar el principio");

	//cout<<"--------------------Prueba eliminar elemento fin-------------------"<<endl<<endl<<endl;
	
	l->BorrarMaximo();

	cout << *l <<endl;
	ver3("Imprimo la lista luego de eliminar el fin");

	//cout<<"--------------------Prueba eliminar elemento-------------------"<<endl<<endl<<endl;
	
	l->Borrar(3);

	cout << *l <<endl;
	ver3("Imprimo la lista luego de eliminar el elemento 3");

	
	//cout<<"--------------------Prueba recuperar elemento-------------------"<<endl<<endl<<endl;
	
	cout << l->Recuperar(2)<<endl;
	
	ver3("Imprimo la recuperación del elemento 2");

	//cout<<"---------------------Prueba elemento pertenece ---------------------"<<endl<<endl<<endl;
	if(l->Existe(2))
	{
		cout<<"Elemento 2 pertenece  a la lista"<<endl<<endl<<endl;
	}else
	{
		cout<<"Elemento 2 no pertenece a la lista"<<endl<<endl<<endl;
	}
	ver3("Imprimo si el elemento 2 existe en la lista");
	//cout<<"---------------------Prueba elemento no pertenece ---------------------"<<endl<<endl<<endl;
	if(l->Existe(6))
	{
		cout<<"Elemento 6 pertenece  a la lista"<<endl<<endl<<endl;
	}else
	{
		cout<<"Elemento 6 no pertenece a la lista"<<endl<<endl<<endl;
	}

	ver3("Imprimo si el elemento 6 existe en la lista");
	//cout<<"-------------------Prueba lista es vacia ----------------------"<<endl<<endl<<endl;
	
	if(l->EsVacia())
	{
		cout<<"La lista se encuentra vacia"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista no se encuentra vacia"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista es vacia");
	/////////////////////////////////////////////////////////////////////////
	ListaOrd<int>* lVacia= l->CrearVacia();
	if(lVacia->EsVacia())
	{
		cout<<"La lista se encuentra vacia"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista no se encuentra vacia"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista creada es vacia ");
	delete lVacia;
	/////////////////////////////////////////////////////////////////////////
	if(l->EsLlena())
	{
		cout<<"La lista se encuentra llena"<<endl<<endl<<endl;
	}
	else
	{
		cout<<"La lista no se encuentra llena"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista esta llena");

	//cout<<"-------------------Prueba lista es vaciar ----------------------"<<endl<<endl<<endl;
	
	l->Vaciar();
	
	cout<<"["<<*l<<"]"<<endl<<endl<<endl;

	ver3("Imprimo la lista luego de vaciarla");

	//cout<<"-------------------Prueba lista es vacia ----------------------"<<endl<<endl<<endl;
	
	if(l->EsVacia())
	{
		cout<<"La lista se encuentra vacia"<<endl<<endl<<endl;
	}else
	{
		cout<<"La lista no se encuentra vacia"<<endl<<endl<<endl;
	}	

	ver3("Imprimo si la lista es vacia");
	delete l;
	delete clonL;

	this->mImpresion.cerrarSeccion("PRUEBAS LISTA ORD");
}

void PruebasEjemploDeUsoEntrega2::pruebaTabla(){

	this->mImpresion.iniciarSeccion("PRUEBAS TABLA");

	Tabla<Cadena,int>* tablaAlCuadrado = new TablaImp<Cadena,int>();

	//cout << "------------------- Prueba agregar elemento ----------------------" << endl << endl << endl;

	tablaAlCuadrado->Insertar(Cadena("Dos"),4); 
	tablaAlCuadrado->Insertar(Cadena("Tres"),9); 
	tablaAlCuadrado->Insertar(Cadena("Cuatro"),16); 
	tablaAlCuadrado->Insertar(Cadena("Cinco"),25); 
	cout<<*tablaAlCuadrado<<endl;
	ver3("Imprimo la tabla luego de agregar cuatro elementos");

	//cout<<"--------------------Prueba cantidad de elementos-------------------"<<endl<<endl<<endl;
	
	cout << tablaAlCuadrado->CantidadElementos()<<endl;
	ver3("Imprimo la cantidad de elementos en la tabla ");

	
	//cout<<"--------------------Prueba clon de la tabla-------------------"<<endl<<endl<<endl;
	
	
	Tabla<Cadena,int>* tablaClon = tablaAlCuadrado->Clon();

	cout << "Resultado Original: " << endl << *tablaAlCuadrado<< endl << endl;

	cout << "Resultado Clon: " << endl << *tablaClon << endl;

	ver3("Imprimo la tabla y su clon");
	///////////////////////////////////////////////////////////////////////////
	Tabla<Cadena,int>* tablaIgual =  tablaAlCuadrado->CrearVacia();
	*tablaIgual = *tablaAlCuadrado;

	cout << "Resultado Original: " << endl << *tablaAlCuadrado<< endl << endl;

	cout << "Resultado tabla utilizando operador =: " << endl << *tablaIgual << endl;

	ver3("Imprimo la tabla y la tabla obtenida");
	//cout<<"--------------------Prueba == de la tabla (iguales)-------------------"<<endl<<endl<<endl;

	if(*tablaAlCuadrado==*tablaClon)
	{
		cout<<"La tabla original es igual a la tabla clon"<<endl<<endl<<endl;
	}else
	{
		cout<<"La tabla original no es igual a la tabla clon"<<endl<<endl<<endl;
	}
	ver3("Imprimo prueba de == sobre tablas iguales");

	//cout<<"--------------------Prueba == de la table (diferentes)-------------------"<<endl<<endl<<endl;
	tablaClon->Insertar(Cadena("Seis"),36);

	if(*tablaAlCuadrado==*tablaClon)
	{
		cout<<"La tabla original es igual a la tabla clon"<<endl<<endl<<endl;
	}else
	{
		cout<<"La tabla original no es igual a la tabla clon"<<endl<<endl<<endl;
	}

	ver3("Imprimo prueba de == sobre tablas distintas");

		
	//cout<<"--------------------Prueba esta definido (elemento definido) -------------------"<<endl<<endl<<endl;
	
	if(tablaAlCuadrado->EstaDefinida("Dos")){
		cout<<"Dos se encuentra definido en la tabla" <<endl;
	}else{
		cout<<"Dos no se encuentra definido en la tabla" <<endl;
	}
	ver3("Imprimo si Dos se encuentra definido en la tabla");

	
	//cout<<"--------------------Prueba esta definido (elemento no definido)-------------------"<<endl<<endl<<endl;
	
	if(tablaAlCuadrado->EstaDefinida("Uno")){
		cout<<"Uno se encuentra definido en la tabla" <<endl;
	}else{
		cout<<"Uno no se encuentra definido en la tabla" <<endl;
	}
	ver3("Imprimo si Uno se encuentra definido en la tabla");

	
	//cout<<"--------------------Prueba recuperar -------------------"<<endl<<endl<<endl;
	
	cout<< tablaAlCuadrado->Recuperar("Dos")<<endl;
	
	ver3("Imprimo la recuperación del elemento Dos");

//	cout<<"--------------------Prueba borrar -------------------"<<endl<<endl<<endl;
	
	tablaAlCuadrado->Borrar("Tres");
	cout<< *tablaAlCuadrado<<endl;
	
	ver3("Imprimo la tabla luego de borrar el elemento Tres");

	
	//cout<<"--------------------Prueba es vacia-------------------"<<endl<<endl<<endl;
	
	if(tablaAlCuadrado->EsVacia()){
		cout<<"La tabla se encuentra vacia" <<endl;
	}else{
		cout<<"La tabla no se encuentra vacia" <<endl;
	}

	ver3("Imprimo si la tabla es vacia");
	///////////////////////////////////////////////////////////////////////////////////

	Tabla<Cadena,int>* tablaCrearVacia =  tablaAlCuadrado->CrearVacia();
	if(tablaCrearVacia->EsVacia()){
		cout<<"La tabla se encuentra vacia" <<endl;
	}else{
		cout<<"La tabla no se encuentra vacia" <<endl;
	}

	ver3("Imprimo si la tabla creada  es vacia");
	
	///////////////////////////////////////////////////////////////////////////////////
	if(tablaCrearVacia->EsLlena()){
		cout<<"La tabla se encuentra llena" <<endl;
	}else{
		cout<<"La tabla no se encuentra llena" <<endl;
	}

	ver3("Imprimo si la tabla creada  se encuentra llena");
	delete tablaCrearVacia;
	//cout<<"--------------------Prueba vaciar -------------------"<<endl<<endl<<endl;
	
	tablaAlCuadrado->Vaciar();
	cout<<"["<< *tablaAlCuadrado<<"]"<<endl;
	
	ver3("Imprimo la tabla luego de vaciarla");

	//cout<<"--------------------Prueba es vacia-------------------"<<endl<<endl<<endl;
	
	if(tablaAlCuadrado->EsVacia()){
		cout<<"La tabla se encuentra vacia" <<endl;
	}else{
		cout<<"La tabla no se encuentra vacia" <<endl;
	}

	ver3("Imprimo si la tabla es vacia");

	delete tablaClon;
	delete tablaAlCuadrado;
	this->mImpresion.cerrarSeccion("PRUEBAS TABLA");

}


void PruebasEjemploDeUsoEntrega2::pruebaColaPrioridad()
{
	this->mImpresion.iniciarSeccion("PRUEBAS COLA PRIORIDAD");

	ColaPrioridad<Cadena, int>* cp = new ColaPrioridadImp<Cadena, int>();

	//cout << "------------------- Prueba cola prioridad agregar ----------------------" << endl << endl << endl;
	Cadena nombre = "A";
	Cadena otroNombre = "C";
	for (int i = 0; i < 10; i += 2)
	{
		Cadena agrego = "A";
		Cadena agrego2 = "C";
		cp->Encolar(nombre, i);
		cp->Encolar(otroNombre, i);
		nombre = nombre + agrego;
		otroNombre = otroNombre + agrego2;
	}
	nombre = "B";
	for (int i = 1; i < 10; i += 2)
	{
		Cadena agrego = "B";
		cp->Encolar(nombre, i);
		nombre = nombre + agrego;
	}

	cout << *cp << endl;
	ver3("Imprimo la cola de prioridad luego de agregar 10 datos");

	// prueba elminar elemento
	cout << cp->PrincipioPrioridad() << " == 9" << endl;
	cp->Desencolar();
	cout << *cp << endl;
	ver3("Imprimo la cola de prioridad luego de eliminar elemento");

	//cout << "------------------- Prueba cola prioridad no vacio ----------------------" << endl << endl << endl;

	Cadena c = cp->EsVacia() ? "True" : "False";
	cout << "Es Vacia: " << c << " == False" << endl;
	ver3("Muestro que la cola de prioridad no es vacia");

	//cout << "---------------------Prueba la operacion asignacion cola prioridad ----------------------" << endl << endl << endl;

	ColaPrioridad<Cadena, int> *asg = new ColaPrioridadImp<Cadena, int>();
	*asg = *cp;
	cout << "Resultado Original: " << endl << *cp << endl;
	cout << "Resultado Clon: " << endl << *asg << endl;
	ver3("Asigno una nueva cola y imprimo la original y la asignada");

	//cout << "---------------------Prueba Clon cola prioridad ----------------------" << endl << endl << endl;

	ColaPrioridad<Cadena, int> *clon = cp->Clon();

	cout << "Resultado Original: " << endl << *cp << endl;
	cout << "Resultado Clon: " << endl << *clon << endl;
	ver3("Clono e imprimo ambas colas de prioridad");

	//cout << "---------------------Prueba == cola prioridad cuando iguales ----------------------" << endl << endl << endl;

	c = (*cp == *clon) ? "True" : "False";
	cout << "Es Igual: " << c << " == True" << endl;
	ver3("Muestro que la cola de prioridad original y la clon son iguales");

	//cout << "---------------------Prueba == cola prioridad cuando diferentes ----------------------" << endl << endl << endl;

	cp->Desencolar();
	c = (*cp == *clon) ? "True" : "False";
	cout << "Es Igual: " << c << " == False" << endl;
	ver3("Muestro que la cola de prioridad original y la clon son diferentes luego de desencolar");

	//cout << "---------------------Prueba vaciar cola prioridad ----------------------" << endl << endl << endl;
	cp->Vaciar();

	cout << *cp << endl;
	c = cp->EsVacia() ? "True" : "False";
	cout << "Es Vacia: " << c << " == True" << endl; 
	ver3("Muestro que la cola de prioridad es vacia luego de usar vaciar y la imprimo"); 


	//cout << "---------------------Prueba Crear Vacia cola prioridad ----------------------" << endl << endl << endl;

	cout << *cp << endl;
	ColaPrioridad<Cadena, int> *vacia = cp->CrearVacia();
	c = vacia->EsVacia() ? "True" : "False";
	cout << "La nueva cola es Vacia: " << c << " == True" << endl;
	ver3("Creo una cola de prioidad usando CrearVacia y muestro que es vacia");

	//cout << "--------------Prueba Largo cola prioridad --------------" << endl << endl << endl;

	cout << "Resultado Cola Original: " << cp->Largo() << " == 0" << endl;
	cout << "Resultado Cola Clon: " << clon->Largo() << " == 14" << endl;
	ver3("Muestro el largo de la cola original y la clonada");

	//cout << "--------------Prueba Obtener Elementos cola prioridad --------------" << endl << endl << endl;

	while (!clon->EsVacia())
	{
		cout << clon->PrincipioElemento() << " - " << clon->PrincipioPrioridad() << endl;
		clon->Desencolar();
	}
	ver3("Imprimo todos los elementos y su priordad del clon");


	cp->Vaciar();

	//cout << "--------------Encolar elementos desde mayor prioridad a la menor, 
	//------------- prueba que elementos siempre vayan al final, y actua como cola normal --------------" << endl << endl << endl;

	Cadena vecAgregar[4] = {"A", "B", "C", "D"};

	for (int i = 3; i >= 0; i--)
	{
		cp->Encolar(vecAgregar[3 - i], i);
	}

	Cadena vecResultado[4];

	int indiceVecResultado = 0;
	while (!cp->EsVacia()) {
		vecResultado[indiceVecResultado++] = cp->PrincipioElemento();
		cp->Desencolar();
	}

	c = (*vecAgregar == *vecResultado) ? "True" : "False";
	cout << "Es Igual: " << c << " == True" << endl;
	ver3("Muestro que se desencolan elementos en mismo orden que se encolan");


	delete cp;
	delete clon;
	delete vacia;
	delete asg; 

	this->mImpresion.cerrarSeccion("PRUEBAS COLA PRIORIDAD");
}

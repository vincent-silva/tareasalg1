#ifndef PRUEBAS_AUX_ENTREGA2_H
#define PRUEBAS_AUX_ENTREGA2_H


#include "FuncAux.h"
#include "Entrega2.h"
#include "FuncAuxTAD.h"


void pruebaContarOcurrencias(const char *lista);
void pruebaSimetricas(const char* inputTabla1Dominio, const char* inputTabla1Rango, const char* inputTabla2Dominio, const char* inputTabla2Rango);
void pruebaMenorPrioridad(const char* inputCpElem, const char* inputCpPrio);
void pruebaParentesisBalanceados(const char *formula);
void pruebaImprimirPorNivelesAG(const char* inputTree);

#endif
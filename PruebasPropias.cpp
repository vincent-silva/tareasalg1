#include "PruebasPropias.h"

PruebasPropias::PruebasPropias(ConductorPrueba* conductor)
:Prueba(conductor)
{
}

PruebasPropias::~PruebasPropias()
{

}

const char* PruebasPropias::getNombre() const
{
	return "PruebasPropias";
}

void PruebasPropias::correrPruebaConcreta()
{

	//// Pruebas Enlistar
	//this->mImpresion.iniciarSeccion("PRUEBAS Enlistar");
	//pruebaEnlistar("{1,5,2}");
	//ver3("Prueba de pasar AB a ListaOrd 1");

	//pruebaEnlistar("{1,#,2,#,5}");
	//ver3("Prueba de pasar AB a ListaOrd 2");

	//pruebaEnlistar("{1,2,3,1,#,#,#,#,3,1,2,1,1,#,#,#,#,#,4,1,#,2,2}");
	//ver3("Prueba de pasar AB a ListaOrd 3");
	//this->mImpresion.cerrarSeccion("PRUEBAS Enlistar");


	//// Pruebas UnionListaOrd
	//this->mImpresion.iniciarSeccion("PRUEBAS UnionListaOrd");
	//pruebaUnionListaOrd("(1,3,5,7,9)", "(2,4,6,8)");
	//ver3("Prueba union de dos listas ordenadas 1");

	//pruebaUnionListaOrd("(100,200,300,400)", "(100,200,300,400)");
	//ver3("Prueba union de dos listas ordenadas 2");

	//pruebaUnionListaOrd("()", "(-1,0,1,5)");
	//ver3("Prueba union de dos listas ordenadas 3");
	//this->mImpresion.cerrarSeccion("PRUEBAS UnionListaOrd");


	//// Pruebas EstaContenida
	//this->mImpresion.iniciarSeccion("PRUEBAS EstaContenida");
	//pruebaEstaContenida("(1,7,4,2)", "(7,3,2,1)");
	//ver3("Prueba de pila contenida 1");

	//pruebaEstaContenida("(1,7,4,2)", "(7,3,2,1,4,8)");
	//ver3("Prueba de pila contenida 2");

	//pruebaEstaContenida("(1,7,4,2,7,4,4,7)", "(7,3,7,2,4,1,4,8)");
	//ver3("Prueba de pila contenida 3");

	//pruebaEstaContenida("(1,7,4,2,7,4,4,7)", "(7,3,7,2,4,4,4,7,1,4,8)");
	//ver3("Prueba de pila contenida 4");
	//this->mImpresion.cerrarSeccion("PRUEBAS EstaContenida");


	//// Pruebas CantidadDeHojas
	//this->mImpresion.iniciarSeccion("PRUEBAS CantidadDeHojas");
	//pruebaCantidadDeHojas("{1,5,2}");
	//ver3("Prueba contar hojas AB iterativo 1");

	//pruebaCantidadDeHojas("{1,#,2,#,5}");
	//ver3("Prueba contar hojas AB iterativo 2");

	//pruebaCantidadDeHojas("{1,2,3,1,#,#,#,#,3,1,2,1,1,#,#,#,#,#,4,1,#,2,2}");
	//ver3("Prueba contar hojas AB iterativo 3");
	//this->mImpresion.cerrarSeccion("PRUEBAS CantidadDeHojas");


	//// Pruebas ImprimirPorNiveles
	//this->mImpresion.iniciarSeccion("PRUEBAS ImprimirPorNiveles");
	//pruebaImprimirPorNiveles("{1,5,2}");
	//ver3("Prueba imprimir AB por niveles 1");

	//pruebaImprimirPorNiveles("{1,#,2,#,5}");
	//ver3("Prueba imprimir AB por niveles 2");

	//pruebaImprimirPorNiveles("{1,2,3,1,#,#,#,#,3,1,2,1,1,#,#,#,#,#,4,1,#,2,2}");
	//ver3("Prueba imprimir AB por niveles 3");
	//this->mImpresion.cerrarSeccion("PRUEBAS ImprimirPorNiveles");
}
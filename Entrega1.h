#ifndef ENTREGA1_H
#define ENTREGA1_H

#include "Definiciones.h"

#include "ListaOrd.h"
#include "ListaPos.h"
#include "Pila.h"
#include "Cola.h"

#include "ListaOrdImp.h"
#include "ListaPosImp.h"
#include "PilaImp.h"
#include "ColaImp.h"

// Ver Entrega1.txt para la documentación de estas funciones


template <class T>
ListaOrd<T> *UnionListaOrd(const ListaOrd<T> &l1, const ListaOrd<T> &l2)
{
	ListaOrd<T>* respuesta;
	ListaOrd<T>* aux;
	if (!l1.EsVacia() && !l2.EsVacia()) {
		if (l1.Maximo() <= l2.Minimo()) {
			respuesta = l1.Clon();
			aux = l2.Clon();
		}
		else {
			respuesta = l2.Clon();
			aux = l1.Clon();
		}

		for (Iterador<T> i = aux->GetIterador(); !i.EsFin();) {
			respuesta->AgregarOrd(i++);
		}
	}
	else if (l1.EsVacia()) {
		respuesta = l2.Clon();
	}
	else {
		respuesta = l1.Clon();
	}
	return respuesta;	
}

ListaOrd<int>* Enlistar(NodoAB* a);

template <class T>
bool EstaContenida(const Pila<T> &p1, const Pila<T> &p2)
{
	Pila<T> * pila1 = p1.Clon();
	Pila<T> * pila2 = p2.Clon();
	ListaPos<T> * busq = new ListaPosImp<T>();
	bool respuesta = p1.EsVacia() || !p2.EsVacia();
	if (!p1.EsVacia() && !p2.EsVacia()) {
		while (!pila2->EsVacia()) {
			busq->AgregarFin(pila2->Pop());
		}

		while (!pila1->EsVacia())
		{
			T elem = pila1->Pop();
			respuesta = respuesta && busq->Existe(elem);
			busq->Borrar(elem);
		}
	}
	return respuesta;
}

unsigned int CantidadDeHojas(NodoAB* a);

void ImprimirPorNiveles(NodoAB *a);

#endif